# Escaner de notas

Busca notas de los usuarios, usado para saber que estudiantes tienen notas asignadas en un curso

## Uso

```bash
tsc main.ts --lib ES2015
node main.js
```

_Desarrollado por Julian David_
**2017 Nov 7**