import usersIds from "./data"
import axios from 'axios'

const base: string = 'https://virtual.udla.edu.co:444/api/public/index.php/'
declare var console: any

interface ICurso {
	id: string,
	idnumber: string,
	summary: string,
	category: string,
	shortname: string,
	programa: string,
	semestre: string,
	fullname: string,
	user: string,
	timecreated: string,
	Rol: string
}

interface IResponse {
	data: Array<{}>
	method: string
	params: Array<string>
	error: boolean
	date: string
}

async function getNotas(idUser: number | string, idCourse: number | string) {
	let url = `${base}dynamic/query/12/${idUser}/${idCourse}`
	let { data, status } = await axios.get(url)
	if (status == 200) {
		let d = data.data
		// console.log("Notas", d)
		return d
	}
	return []
}

async function getCourses(idUser: any) {
	let url = `${base}dynamic/query/11/${idUser}`
	let { data, status } = await axios.get(url)

	if (status == 200) {
		// console.log("Cursos:", data)
		return (data as IResponse).data as ICurso[]
	}
	return []
}

async function run() {
	usersIds.map(async (idUser, index) => {
		let idu = idUser.id
		// console.log(`Escaneando ${idu}`)

		let courses = await getCourses(idu)
		courses.map(async (c) => {
			console.log(`Escaneando Usuario(${idu}) en el curso ${c.id}`)
			let notas = await getNotas(idu, c.id)
			if (notas && notas.length > 0) {
				console.log(`Notas para ${idu} - ${c.id}`, notas)
			}
		})
	})
}

run()